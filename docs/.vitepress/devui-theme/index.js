import './styles/base.scss';
import './styles/vars.scss';
import './styles/layout.scss';
import './styles/code.scss';
import './styles/custom-blocks.scss';
import './styles/sidebar-links.scss';
import '@devui-design/icons/icomoon/devui-icon.css';
import Layout from './Layout.vue';
import NotFound from './NotFound.vue';
const theme = {
    Layout,
    NotFound
};
export default theme;
